<?php

use Illuminate\Support\Facades\Route;
use App\User;
use App\Household;
use App\Hhrequest;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// INSERT
Route::get('/insert', function (){
    $user = new User();
    $user->name = "krisz";
    $user->email = "krisz@email.hu";
    $user->password = "jelszo";
    $user->save();

    $user = new User();
    $user->name = "eszti";
    $user->email = "eszti@email.hu";
    $user->password = "jelszo";
    $user->save();

    $hh = new Household();
    $hh->name = "Otthon1";
    $hh->save();

    $hh = new Household();
    $hh->name = "Otthon2";
    $hh->save();

    $hh = new Household();
    $hh->name = "Otthonka1";
    $hh->save();

    $hh = new Household();
    $hh->name = "Otthonka2";
    $hh->save();

    $hh = new Household();
    $hh->name = 'Otthonka23';
    $hh->save();

    $hhh = new Hhrequest();
    $hhh->user_id = 1;
    $hhh->household_id = 1;
    $hhh->save();

    $hhh = new Hhrequest();
    $hhh->user_id = 1;
    $hhh->household_id = 2;
    $hhh->save();

    $hhh = new Hhrequest();
    $hhh->user_id = 1;
    $hhh->household_id = 3;
    $hhh->save();

    $hhh = new Hhrequest();
    $hhh->user_id = 2;
    $hhh->household_id = 3;
    $hhh->save();

    $hhh = new Hhrequest();
    $hhh->user_id = 2;
    $hhh->household_id = 4;
    $hhh->save();
});

    // getting all the households where the user is inside
//Route::get('/user/{id}', function($id) {
//    $user = User::find($id)->households()->get();
//    return $user;
//});

    // getting all the request sent by the user
Route::get('/user/{from}', function($from) {
    $user = User::find($from)->hhrequests()->get();
    return $user;
});
