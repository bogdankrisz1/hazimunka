<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Household extends Model
{
    //
    protected $primaryKey = 'household_id';
}
