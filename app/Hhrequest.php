<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hhrequest extends Model
{
    //
    protected $primaryKey = 'request_id';

    protected $attributes = [
        'status_id' => 0
    ];
}
