<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    // user_id instead of id (inside of users table)
    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // pivot table between Households and Users
    public function households()
    {
        return $this->belongsToMany('App\Household', 'household_user', 'user_id', 'household_id');
    }

    public function hhrequests()
    {
        return $this->belongsToMany('App\Hhrequest', 'hhrequest_user', 'user_id', 'request_id');
    }
}
